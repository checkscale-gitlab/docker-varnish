# Varnish in a Docker container

[![pipeline status](https://gitlab.com/florenttorregrosa-docker/images/docker-varnish/badges/develop/pipeline.svg)](https://gitlab.com/florenttorregrosa-docker/images/docker-varnish/-/commits/develop)

This repo contains the build for the maintained docker container https://hub.docker.com/r/florenttorregrosa/varnish.

## Use

### Sample docker-compose entry

A sample docker-compose file for this container is as follows:

```
version: '3.4'
services:
  varnish:
    image: florenttorregrosa/varnish:6-alpine
    volumes:
      - ./conf/varnish:/varnish-drupal:delegated
    environment:
      VCL_CONFIG: /varnish-drupal/varnish.vcl
    depends_on:
      - web_server
```
